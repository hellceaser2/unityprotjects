﻿using UnityEngine;
using System.Collections;

namespace SA
{
    public class AnimHashes
    {
        public int vertical = Animator.StringToHash("vertical");
        public int horizontal = Animator.StringToHash("horizontal");
    }
}